<?php

function checkFilename($filename)
{
    if (!preg_match('/.+\.md$/', $filename)) {
        throw new Exception("Filename must end with .md");
    }

    if (basename($filename) != $filename) {
        throw new Exception("File must be in the root folder");
    }
}

function getAmountFromText($filename)
{
    $contents = preg_split('/\r?\n?---\r?\n/m', file_get_contents($filename));
    if (sizeof($contents) < 3) {
        throw new Exception("Failed to parse proposal, can't find YAML description surrounded by '---' lines");
    }
    return yaml_parse($contents[1]);
}

function checkMandatoryFields($values, $mandatoryFields)
{
    foreach ($mandatoryFields as $field) {
        if (empty($values[$field])) {
            throw new Exception("Mandatory field $field is missing");
        }
    }
}

function checkLayout($layout, $layoutToState)
{
    if (!array_key_exists($layout, $layoutToState)) {
        throw new Exception("Invalid layout field value");
    }
}

function checkMilestones($milestones)
{
    if (!is_array($milestones)) {
        throw new Exception("Milestones should be an array");
    }
    foreach ($milestones as $milestone) {
        if (!isset($milestone['done'])) {
            throw new Exception("Each milestone must have a 'done' field");
        }
    }
}

function validateProposal($filename)
{
    $layoutToState = [
        'fr'    => 'FUNDING-REQUIRED',
        'wip'   => 'WORK-IN-PROGRESS',
        'cp'    => 'COMPLETED'
    ];

    $mandatoryFields = [
        'amount',
        'author',
        'date',
        'layout',
        'milestones',
        'title'
        //'payout' not mandatory
    ];

    checkFilename($filename);
    $values = getAmountFromText($filename);
    checkMandatoryFields($values, $mandatoryFields);
    checkLayout($values['layout'], $layoutToState);
    checkMilestones($values['milestones']);
    return $values;
}

function checkMergeRequest($mergeRequest, $existingProjects, $ideas)
{
    $newFiles = $mergeRequest['files'];
    if (count($newFiles) != 1) {
        throw new Exception("Skipping MR #{$mergeRequest['id']} '{$mergeRequest['title']}': contains multiple files");
    }

    $filename = $newFiles[0];
    checkFilename($filename);

    if (in_array(strtolower($filename), array_map('strtolower', $ideas))) {
        throw new Exception("Skipping MR #{$mergeRequest['id']} '{$mergeRequest['title']}': duplicated $filename, another MR #{$ideas[$filename]['id']}");
    }

    if (isset($existingProjects[strtolower($filename)])) {
        throw new Exception("Skipping MR #{$mergeRequest['id']} '{$mergeRequest['title']}': already have a project $filename");
    }

    return $filename;
}

try {
    // Retrieve the filename from the command line argument
    if ($argc !== 2) {
        throw new Exception("Usage: php proposal_validator.php <filename>");
    }
    
    $mergeRequestFile = $argv[1];

    $existingProjects = [];
    $ideas = [];

    // Check if the provided filename already exists (case-insensitive)
    foreach (glob("*.md") as $file) {
        if (strcasecmp($file, $newFile) === 0) {
            echo "Error: A file with the name $newFile already exists in the current directory (case-insensitive).\n";
            exit(1);
        }
    }

    // Validate the provided markdown file from the merge request
    $proposalValues = validateProposal($mergeRequestFile);
    echo "Proposal $mergeRequestFile is valid. Details:\n";
    print_r($proposalValues);
} catch (Exception $e) {
    echo "Validation error: " . $e->getMessage() . "\n";
    exit(1);
}

echo "Proposal validation succeeded.\n";
exit(0);
