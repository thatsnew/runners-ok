#!/bin/bash

set -e

# Function to strip YAML front matter
strip_yaml_front_matter() {
  awk '
  /^---$/ { 
    if (!in_yaml) { 
      in_yaml=1; next 
    } else { 
      in_yaml=0; next 
    } 
  } 
  !in_yaml { print }'
}

# Function to check for YAML front matter
has_yaml_front_matter() {
  awk '
  BEGIN { yaml_start=0; yaml_end=0 } 
  /^---$/ { 
    if (!yaml_start) { 
      yaml_start=1 
    } else { 
      yaml_end=1 
    } 
  } 
  END { exit !(yaml_start && yaml_end) }' "$1"
}
echo "Fetching merge request description from GitLab API..."
MR_DESCRIPTION=$(curl -s --fail "$CI_API_V4_URL/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID" | jq -r '.description')
if [ $? -ne 0 ]; then
  echo "Failed to fetch the merge request description."
  exit 1
fi

echo "Merge request description:"
echo "$MR_DESCRIPTION"

echo "Stripping YAML front matter from the merge request description..."
MR_DESCRIPTION_CONTENT=$(echo "$MR_DESCRIPTION" | strip_yaml_front_matter)
echo "Merge request description content (after stripping YAML front matter):"
echo "$MR_DESCRIPTION_CONTENT"

if [ -z "$MR_DESCRIPTION_CONTENT" ]; then
  echo "Error: The merge request description is empty after stripping YAML front matter."
  exit 1
fi

echo "Fetching the main branch from the remote repository..."
git fetch origin main || {
  echo "Failed to fetch the main branch from the remote repository."
  exit 1
}

echo "Fetching new files in the merge request..."
NEW_FILES=$(git diff --diff-filter=A --name-only "origin/main...$CI_MERGE_REQUEST_TARGET_BRANCH")
if [ $? -ne 0 ]; then
  echo "Failed to fetch new files in the merge request."
  exit 1
fi

echo "List of new files in the merge request:"
echo "$NEW_FILES"

if [ $(echo "$NEW_FILES" | wc -l) -ne 1 ]; then
  echo "Error: The merge request must contain exactly one new file."
  exit 1
fi

NEW_FILE=$(echo "$NEW_FILES" | head -n 1)
echo "New file in the merge request:"
echo "$NEW_FILE"

echo "Checking if the Markdown file $NEW_FILE contains YAML front matter..."
if ! has_yaml_front_matter "$NEW_FILE"; then
  echo "Error: The Markdown file $NEW_FILE does not contain YAML front matter."
  exit 1
fi

FILE_CONTENT=$(cat "$NEW_FILE" | strip_yaml_front_matter)
echo "Content of the Markdown file $NEW_FILE (after stripping YAML front matter):"
echo "$FILE_CONTENT"

DIFF=$(diff <(echo "$MR_DESCRIPTION_CONTENT") <(echo "$FILE_CONTENT") || true)
if [ -n "$DIFF" ]; then
  echo "Error: The merge request description does not match the content of the Markdown file $NEW_FILE."
  echo "Differences:"
  echo "$DIFF"
  exit 1
fi

echo "Validating the Markdown file $NEW_FILE..."
php .gitlab/scripts/proposal_validator.php "$NEW_FILE"
